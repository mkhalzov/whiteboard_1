var express = require('express');
var app = express();

//app.use(express.static(__dirname)); // Current directory is root
app.use(express.static(__dirname + '/public')); //  "public" off of current is root

app.listen(3033);
console.log('Listening on port 3033');