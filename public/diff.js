function diff() {
	var beforeEl = document.getElementById("before");
	var afterEl = document.getElementById("after");

	var output = document.getElementById("output");

	function getNodes(el) {
		return el.innerHTML.split("\n")
			.map(function(val, index) {
				return val.trim()
			})
			.filter(function(val) {
				return !!val
			}
		);
	};

	var before = getNodes(beforeEl);
	var after = getNodes(afterEl);

	output.innerHTML += '<br>============<br>';
	output.innerHTML += before.join('<br>');
	output.innerHTML += '<br>------------<br>';
	output.innerHTML += after.join('; ');
	output.innerHTML += '<br>============<br>';
}